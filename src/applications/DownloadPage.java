package applications;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import main.DesktopEnvironment;

public class DownloadPage extends WindowedApplication {

	private static final long serialVersionUID = 1L;

	public DownloadPage() {
		this.setBackground(Color.WHITE);
		int width = 600; int height = 580;
		this.setLayout(null);
		this.setLocation(0, 600-height);
		JLabel title = new JLabel("<html><h1>Collisions Lab: Ball Simulator 2000</h1></html>");
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setBounds(0, 0, 600, 50);
		this.add(title);
		JLabel header = new JLabel("<html><h2>Ball Simulator 2000</h2></html>");
		header.setBounds(0, 50, 400, 50);
		this.add(header);
		JLabel description = new JLabel("<html>The Ball Simulator 2000 is a fully-fledged interactive strategy turn-based action MMORPG ball simulator, " +
										"complete with intense gameplay and thrilling action. The game's sophisticated mechanics introduces " +
										"novel and interactive ways to progress the game, surpassing even games such as The Witcher 3 and Legend of Zelda: Breath of the Wild. " +
										"It implements the latest video game technology, from advanced shaders to ray tracing to low-poly sprites. " + 
										"With its stunning graphcis and vast ocean of features, levels, and content, the Ball Simulator 2000 is sure to satisfy all your gaming needs. " + 
										"Of course, the game is full of gambling and lootboxes in order to give every player a strong sense of pride and accomplishment. " + 
										"The game also comes with a fully orchestral OST, sure to please even the most seasoned ears. To download, click below: </html>");
		description.setFont(new Font("Dialog", Font.PLAIN, 13));
		description.setBounds(0, 100, 400, 300);
		this.add(description);
		BufferedImage buttonIcon = null;
		BufferedImage buttonIcon2 = null;
		BufferedImage buttonIcon3 = null;
		try {
			buttonIcon = ImageIO.read(getClass().getResource("/resources/download.png"));
			buttonIcon2 = ImageIO.read(getClass().getResource("/resources/downloadnow.jpg"));
			buttonIcon3 = ImageIO.read(getClass().getResource("/resources/bigdownload.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JButton[] buttons = new JButton[4];
		buttons[0] = new JButton(new ImageIcon(buttonIcon));
		buttons[0].setBounds(400, 50, 200, 200);
		//bad_button1.setBorder(BorderFactory.createEmptyBorder());
		buttons[0].setToolTipText("downloadmanager.com/download.php");
		buttons[1] = new JButton(new ImageIcon(buttonIcon2));
		buttons[1].setBounds(400, 250, 200, 100);
		buttons[1].setSize(200, 100);
		buttons[1].setBorder(BorderFactory.createEmptyBorder());
		buttons[1].setToolTipText("s74hjkdfasd.ru/zkjn4onofd.php");
		buttons[1].setBackground(Color.WHITE);
		buttons[2] = new JButton(new ImageIcon(buttonIcon3));
		buttons[2].setBounds(60, 450, 480, 125);
		buttons[2].setBackground(Color.WHITE);
		buttons[2].setToolTipText("adf.ly/j43Ix");
		buttons[3] = new JButton("Click here to download");
		buttons[3].setToolTipText("collisionslab.org/static/ballsimulator.msi");
		buttons[3].setBackground(Color.WHITE);
		buttons[3].setForeground(Color.BLUE);
		buttons[3].setBounds(0, 375, 600, 50);
		buttons[3].setBorderPainted(false);
		buttons[3].setOpaque(false);
		
		for (int i = 0; i < buttons.length; i++) {
			this.add(buttons[i]);
			if (i == 3) {
				buttons[i].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						start(new Installer());
					}
				});
			} else {
				addListener(buttons[i]);
			}
		}
	}
	
	public void addListener(JButton b) {
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				window.add_mistake("Incorrectly clicked download button leading to url " + b.getToolTipText());
			}
		});
	}
}
