package main;


import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

import applications.AntivirusMessage;
import applications.BouncingBall;
import applications.EndDialog;
import applications.IntroDialog;
import applications.TerminalEmulator;
import applications.WindowedApplication;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;


public class DesktopEnvironment extends JPanel {

	private static final long serialVersionUID = 1L;
//	private int posX = 200, posY = 200;
	private ArrayList<String> mistakes = new ArrayList<String>(); // bad style but who cares tbh
	JPanel taskbar = new JPanel();
	JDesktopPane desktop = new JDesktopPane();
	final int TASKBAR_HEIGHT = 40;
	private boolean easyMode = true;
	private int score = 100;

	public DesktopEnvironment() {
		setPreferredSize(new Dimension(800, 600));
		desktop.setBackground(new Color(0, 150, 200));
		
		setLayout(new BorderLayout());
		taskbar.setLayout(new FlowLayout());
		taskbar.setBackground(Color.BLUE);
		taskbar.setPreferredSize(new Dimension(0, 50));
		add(taskbar, BorderLayout.SOUTH);
		add(desktop, BorderLayout.CENTER);
	}

	public WindowedApplication add(WindowedApplication application) {
		JInternalFrame window = new JInternalFrame(application.toString(), application.getResizable(), application.getClosable());
		application.setDesktop(this);
		window.setContentPane(application);
		window.pack();
		window.show();
		desktop.add(window, 0);
		System.out.println(application.getWidth() + ", " + application.getHeight());
		window.setLocation((desktop.getWidth() - application.getWidth()) / 2, (desktop.getHeight() - application.getHeight()) / 2);
		return application;
	}

	// adds an application icon to the bottom bar
	public void addApplicationIcon(WindowedApplication application) {
		JButton launcher = new JButton();
		launcher.setPreferredSize(new Dimension(50, TASKBAR_HEIGHT));
		launcher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					add(application.getClass().newInstance());
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		});
		launcher.setText(application.toString().charAt(0) + "");
		taskbar.add(launcher);
		taskbar.revalidate();
		taskbar.repaint();
	}

	// responsible for updating all of the running programs
	public void update() {
		for (Component program : desktop.getComponents()) {
			if (program instanceof JInternalFrame && ((JInternalFrame) program).getContentPane() instanceof WindowedApplication) {
				((WindowedApplication) ((JInternalFrame) program).getContentPane()).update();
			}
		}
	}
	
	public void closeApplication(WindowedApplication application) {
		for (Component frame : desktop.getComponents()) {
			if (frame instanceof JInternalFrame && ((JInternalFrame) frame).getContentPane() == application) {
				desktop.remove(frame);
			}
		}
	}
	
	public void replaceApplication(WindowedApplication oldApp, WindowedApplication newApp) {
		for (Component frame : desktop.getComponents()) {
			if (frame instanceof JInternalFrame && ((JInternalFrame) frame).getContentPane() == oldApp) {
				((JInternalFrame) frame).setContentPane(newApp.setDesktop(this));
			}
		}
	}

	public void userError(String error, String explanation) {
		if (easyMode) {
			add(new AntivirusMessage(error, explanation));
			score -= 10;
		}
	}
	
	public void endGame() {
		add(new EndDialog(score));
	}
	
	public void setEasyMode(boolean b) {
		easyMode = b;
	}

	public void add_mistake(String s) {
		// adds string to global arraylist in main which will be displayed at the end and used to calculate score
		if (!mistakes.contains(s)) {
			mistakes.add(s);
		}
//		Popup.newPopup(s);
	}

	public String toString() {
		return "test";
	}
	
}
