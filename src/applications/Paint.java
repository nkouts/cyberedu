package applications;
// ************************************************************
// Editor.java			Author: Nikodemos Koutsoheras
// 
// Main code for the image editor- takes care of user input and
// drawing to the screen.
// ************************************************************

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class Paint extends WindowedApplication implements MouseListener, MouseMotionListener, ActionListener, KeyListener {

	private static String cType = "Line";
	private static int numLayers;
	
	
	// ------------------------------------------------------------
	// Gets index of nth occurrence of a substring in a string
	// Not my work, written by aioobe
	// ------------------------------------------------------------
	public static int ordinalIndexOf(String str, String substr, int n) {
		int pos = str.indexOf(substr);
		while (--n > 0 && pos != -1)
			pos = str.indexOf(substr, pos + 1);
		return pos;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum Type {
		LINE,
		SLINE,

		RECT,
		ELLIPSE,
		TRIANGLE,
		ARC,

		// "filled"
		F_RECT,
		F_ELLIPSE,
		F_TRIANGLE,
		F_ARC,

		// "filled bordered"
		FB_RECT,
		FB_ELLIPSE,
		FB_TRIANGLE,
		FB_ARC,

		// "freeform curve"
		FFC,
		C_FFC,
		FC_FFC,
		FBC_FFC
	}

	// Cursor coordinates
	private int posX, posY;

	// Stores click data. ClickMax indicates whether the current
	// selected figure takes 2 or 3 positional arguments.
	private int clickNum = 1;
	private int clickMax = 1;
	private int[] x = new int[3];
	private int[] y = new int[3];
	// mod0 indicates state of modifier key 0 (control)
	// mod0flag is made true when mod0 is made false to let code in mouseReleased know that ctrl was being used for the last draw
	private boolean mod0, mod0flag;

	// Stores data about the off screen image
	private int widthOffImage, heightOffImage;

	// Stores color data.
	private Color fillColor = new Color(0, 0, 0);
	private Color lineColor = new Color(0, 0, 0);
	private int[][] colorPalette = {
			{201,77,77},
			{216,131,43},
			{218,183,31},
			{64,98,68},
			{36,37,81},
			{0, 0, 0},
			{255, 255, 255},
			{228, 228, 228},
			{228, 228, 228},
			{228, 228, 228}
	};

	private boolean clrImg = false;



	// current selected tool
	private Type figSelect = Type.LINE;
	private ArrayList<int[]> freeTemp = new ArrayList<int[]>();

	private ArrayList<Layer> layers = new ArrayList<Layer>();
	private int activeLayer = 0;

	//	private ArrayList<String> imageCode = new ArrayList<String>();
	//	private ArrayList<String> colorCode = new ArrayList<String>();

	private Image offImage;

	private DialogueBox dBox;
	private ColorBox cBox;
	private LayerBox lBox;


	// ------------------------------------------------------------
	// Constructor- sets up mouse listeners
	// ------------------------------------------------------------
	public Paint() {
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
		setBackground(Color.white);
		this.setFocusable(true);
		setPreferredSize(new Dimension(200, 200));
		//		setSize(new Dimension (500, 500));

		layers.add(new Layer());

	}

	// ------------------------------------------------------------	
	// Mutator method to rewrite values of the image data
	// ArrayLists (used by LoadDialogue to load a new image)
	// ------------------------------------------------------------
	public void setImageData(ArrayList<Layer> newLayerData) {
		layers = newLayerData;

		redraw(layers);
	}

	public void setColorData(String type, int[] color, int[][] cp) {
		colorPalette = cp;
		if (type.equals("Fill")) {
			fillColor = new Color(color[0], color[1], color[2]);
		} else {
			lineColor = new Color(color[0], color[1], color[2]);
		}
	}

	public void setLayerData(ArrayList<Layer> newLayerList) {
		layers = newLayerList;
	}

	public int getActiveLayer() {
		return activeLayer;
	}

	public void setActiveLayer(int a) {
		activeLayer = a;
	}

	// ------------------------------------------------------------
	// Reloads the page, clearing it first. Meant to be called
	// externally. Consider merging with redraw().
	// ------------------------------------------------------------
	public void updateImage() {
		clrImg = true;
		checkOffImage();
		clrImg = false;
		redraw(layers);
	}


	// ------------------------------------------------------------
	// Manages the off screen image (used to save permanent
	// image data).
	// ------------------------------------------------------------
	// || widthOffImage != getSize().width || heightOffImage != getSize().height 
	private void checkOffImage() {
		if (offImage == null || widthOffImage != getSize().width || heightOffImage != getSize().height || clrImg) {
			offImage = null;
			offImage = createImage(getSize().width, getSize().height);
			widthOffImage = getSize().width;
			heightOffImage = getSize().height;
			redraw(layers);

			//Consider making this permanent
			//			Graphics OffGraphics = offImage.getGraphics();
			//			OffGraphics.setColor(getBackground());
			//			OffGraphics.fillRect(0, 0, widthOffImage, heightOffImage);
			//			OffGraphics.dispose();
		}
	}


	// ------------------------------------------------------------
	// This is used for the "dragging" operation. Draws to the screen
	// without saving to the off screen image.
	// ------------------------------------------------------------
	public void paintComponent(Graphics g) {
		checkOffImage();
		g.drawImage(offImage, 0, 0, this);
		if (clickNum != clickMax && clickNum != -1 && layers.get(activeLayer).isVisible()) {
			if (figSelect != Type.FFC && figSelect != Type.C_FFC && figSelect != Type.FC_FFC && figSelect != Type.FBC_FFC) {
				if (clickMax == 1) {
					drawFigure(g, figSelect, x[1], y[1], posX, posY);
				} else if (clickMax == 2 && clickNum == 1) {
					drawFigure(g, Type.LINE, x[2], y[2], posX, posY);
				} else if (clickMax == 2 && clickNum == 0) {
					drawFigure(g, figSelect, x[2], y[2], x[1], y[1], posX, posY);
				}
			} else
				drawFigure(g, Type.LINE, freeTemp.get(freeTemp.size() - 1)[0], freeTemp.get(freeTemp.size() - 1)[1], posX, posY);
		}
		//		for (int i = activeLayer + 1; i < layers.size(); i++) {
		//			g.drawImage(layers.get(i).getImage(), 0, 0, this);
		//		}
		if (dBox != null) {
			dBox.draw(g);
		}
		if (cBox != null) {
			cBox.draw(g);
		}
		if (lBox != null) {
			lBox.draw(g);
		}

	}

	public void keyPressed (KeyEvent evt) {
		mod0 = evt.isControlDown();
	}

	public void keyReleased(KeyEvent evt) {
		mod0 = evt.isControlDown();
		mod0flag = true;
	}

	public void keyTyped(KeyEvent evt) {
	}


	// ------------------------------------------------------------
	// Unused
	// ------------------------------------------------------------
	public void actionPerformed(ActionEvent arg0) {	
	}
	public void mouseDragged(MouseEvent arg0) {
	}
	public void mouseClicked(MouseEvent arg0) {
	}
	public void mouseEntered(MouseEvent arg0) {
	}
	public void mouseExited(MouseEvent arg0) {
	}
	public void mousePressed(MouseEvent arg0) {
	}

	// ------------------------------------------------------------
	// If a figure is being drawn, updates the position of the
	// cursor and repaints part of the screen.
	// ------------------------------------------------------------
	public void mouseMoved(MouseEvent evt) {



		if (clickNum == -1 || clickNum == clickMax) {
			return;
		}

		if (figSelect.toString().contains("FFC") && layers.size() > 0) {
			if (!mod0) {
				drawFigure(offImage.getGraphics(), 
						Type.LINE, freeTemp.get(freeTemp.size() - 1)[0], freeTemp.get(freeTemp.size() - 1)[1], 
						evt.getX(), evt.getY());
				int[] tempInt = {evt.getX(), evt.getY()};
				freeTemp.add(tempInt);
			}
			repaint();
		}

		posX = evt.getX();
		posY = evt.getY();


		repaintArea(x[clickMax], y[clickMax], x[clickNum], y[clickNum]);
		repaintArea(x[clickMax], y[clickMax], posX, posY);
	}

	// ------------------------------------------------------------
	// Does various actions when the cursor is released.
	// ------------------------------------------------------------
	public void mouseReleased(MouseEvent evt) {	
		posX = evt.getX();
		posY = evt.getY();
		// begin code that manages dialogue box interaction
		if (evt.getButton() == MouseEvent.BUTTON1) {

			if (dBox != null) {
				int temp = clickMax;
				dBox.inputClick(posX, posY);
				switch (dBox.getBoxCode()) {
				case "":
					dBox = null;
					break;
				case "00":
					// choose shape
					break;
				case "01":
					// choose color
					dBox = null;
					cBox = new ColorBox(this, posX, posY, colorPalette, fillColor, lineColor);
					break;
				case "02":
					// choose layer
					dBox = null;
					lBox = new LayerBox(this, layers, posX, posY);
					break;
				case "03":
					// undo
					layers.get(activeLayer).undo();
					clrImg = true;
					checkOffImage();
					clrImg = false;
					break;
				case "04":
					// reload
					clrImg = true;
					checkOffImage();
					clrImg = false;
					redraw(layers);
					dBox = null;
					break;
				case "05":
					// Load image
					// Passes itself to LoadDialogue so LoadDialogue can call
					// the Display object's setImageData method
					LoadDialogue load = new LoadDialogue(this);
					load.Display();
					dBox = null;
					break;
				case "06":
					// Save image
					new SaveDialogue(layers, true).Display(); 
					dBox = null;
					break;
				case "07":
					// Export image (as Java code)
					new SaveDialogue(layers, false).Display(); 
					dBox = null;
					break;
				case "08":
					// Clear image
					layers = new ArrayList<Layer>();

					clrImg = true;
					checkOffImage();
					clrImg = false;
					dBox = null;
					break;
				case "0000":
					figSelect = Type.LINE;
					clickMax = 1;
					dBox = null;
					break;
				case "0001":
					figSelect = Type.SLINE;
					clickMax = 1;
					dBox = null;
					break;
				case "0010":
					figSelect = Type.RECT;
					clickMax = 1;
					dBox = null;
					break;
				case "0011":
					figSelect = Type.F_RECT;
					clickMax = 1;
					dBox = null;
					break;
				case "0012":
					figSelect = Type.FB_RECT;
					clickMax = 1;
					dBox = null;
					break;
				case "0020":
					figSelect = Type.ELLIPSE;
					clickMax = 1;
					dBox = null;
					break;
				case "0021":
					figSelect = Type.F_ELLIPSE;
					clickMax = 1;
					dBox = null;
					break;
				case "0022":
					figSelect = Type.FB_ELLIPSE;
					clickMax = 1;
					dBox = null;
					break;
				case "0030":
					figSelect = Type.TRIANGLE;
					clickMax = 2;
					dBox = null;
					break;
				case "0031":
					figSelect = Type.F_TRIANGLE;
					clickMax = 2;
					dBox = null;
					break;
				case "0032":
					figSelect = Type.FB_TRIANGLE;
					clickMax = 2;
					dBox = null;
					break;
				case "0040":
					figSelect = Type.ARC;
					clickMax = 2;
					dBox = null;
					break;
				case "0041":
					figSelect = Type.F_ARC;
					dBox = null;
					break;
				case "0050":
					figSelect = Type.FFC;
					clickMax = 1;
					dBox = null;
					break;
				case "0051":
					figSelect = Type.C_FFC;
					clickMax = 1;
					dBox = null;
					break;
				case "0052":
					figSelect = Type.FC_FFC;
					clickMax = 1;
					dBox = null;
					break;
				case "0053":
					figSelect = Type.FBC_FFC;
					clickMax = 1;
					dBox = null;
					break;
				}

				clickNum = (temp == clickMax)? clickNum : clickMax;
				repaint();
				return;
			}

			if (cBox != null && cBox.isActive()) {
				cBox.inputClick(posX, posY);
				repaint();
				if (!cBox.isActive()) {
					cBox = null;
				}
				return;
			}

			if (lBox != null && lBox.isActive()) {
				lBox.inputClick(posX, posY);
				repaint();
				if (!lBox.isActive()) {
					lBox = null;
				}
				return;
			}


			// Adds the initial coordinate to the temporary freeform curve array
			// Needed because mouseMoved() will only add the second coord
			if (figSelect.toString().contains("FFC") && clickNum == clickMax) {
				int[] tempInt = {posX, posY};
				freeTemp.add(tempInt);
				repaint();
				if (mod0)
					clickNum--;
			}

			if (figSelect.toString().contains("FFC") && mod0) {
				drawFigure(offImage.getGraphics(), Type.LINE, freeTemp.get(freeTemp.size() - 1)[0], freeTemp.get(freeTemp.size() - 1)[1], evt.getX(), evt.getY());
				int[] tempInt = {evt.getX(), evt.getY()};
				freeTemp.add(tempInt);
				repaint();
			}


			if (!mod0) {
				x[clickNum] = evt.getX();
				y[clickNum] = evt.getY();
				clickNum--;
			}

			if (clickNum == -1) {
				clickNum = clickMax;

				if (clickMax == 1) {
					if (!figSelect.toString().contains("FFC")) {
						if (layers.get(activeLayer).isVisible())
							drawFigure(offImage.getGraphics(), figSelect, x[1], y[1], x[0], y[0]);
						layers.get(activeLayer).addFigure(figSelect.name() + "," + x[1] + "," + y[1]
								+ "," + x[0] + "," + y[0]);
					}
					else {
						if (mod0flag) {
							int[] tempInt = {evt.getX(), evt.getY()};
							freeTemp.add(tempInt);
							mod0flag = false;
						}
						// There's probably a better way to do this. I couldn't figure it out.
						int[][] temp = new int[freeTemp.size()][2];
						for (int i = 0; i < freeTemp.size(); i++)
							temp[i] = freeTemp.get(i);

						layers.get(activeLayer).addFigure(figSelect.name()+ "," + Arrays.deepToString(temp));
						drawFigure(offImage.getGraphics(), figSelect, temp);
						freeTemp = new ArrayList<int[]>();
						repaint();
					}

				}
				else if (clickMax == 2) {
					if (layers.get(activeLayer).isVisible())
						drawFigure(offImage.getGraphics(), figSelect, x[2], y[2], x[1], y[1], x[0], y[0]);
					layers.get(activeLayer).addFigure(figSelect.name() + "," + x[2] + "," + y[2]
							+ "," + x[1] + "," + y[1] + "," + x[0] + "," + y[0]);
				}
				layers.get(activeLayer).addColor(lineColor.getRed() + "," + lineColor.getGreen() + "," + lineColor.getBlue() + "," + 
						fillColor.getRed() + "," + fillColor.getGreen() + "," + fillColor.getBlue());
			}
		}


		// If the middle mouse button is clicked, undoes last action
		else if (evt.getButton() == MouseEvent.BUTTON2) {
			layers.get(activeLayer).undo();
			clrImg = true;
			checkOffImage();
			clrImg = false;
			repaint();
			return;
		}


		// ------------------------------------------------------------
		// This creates a new dialogue box on a right click event.
		// It doesn't actually draw the box here, because if it
		// did, it would be permanent. It creates a dialogue box
		// object which paintComponent will draw later.
		// ------------------------------------------------------------
		else if (evt.getButton() == MouseEvent.BUTTON3) {
			dBox = new DialogueBox(this, offImage.getGraphics(), posX, posY);
			repaint();
		}
	}

	// ------------------------------------------------------------
	// Draws shapes that require 2 positional arguments
	// ------------------------------------------------------------
	private void drawFigure(Graphics g, Type t, int x1, int y1, int x2, int y2) {
		int width, height, x0, y0;
		if (x1 >= x2) {
			x0 = x2;
			width = x1 - x2;
		}
		else {
			x0 = x1;
			width = x2 - x1;
		}
		if (y1 >= y2) {
			y0 = y2;
			height = y1 - y2;
		}
		else {
			y0 = y1;
			height = y2 - y1;
		}

		switch (t) {
		case LINE:
			g.setColor(lineColor);
			g.drawLine(x1,y1,x2,y2);
			break;
		case SLINE:
			g.setColor(lineColor);
			if (Math.abs(x1 - x2) > Math.abs(y1 - y2))
				g.drawLine(x1, y1, x2, y1);
			else
				g.drawLine(x1, y1, x1, y2);
			break;
		case RECT:
			g.setColor(lineColor);
			g.drawRect(x0, y0, width, height);
			break;
		case F_RECT:
			g.setColor(fillColor);
			g.fillRect(x0, y0, width, height);
			break;
		case FB_RECT:
			g.setColor(fillColor);
			g.fillRect(x0, y0, width, height);
			g.setColor(lineColor);
			g.drawRect(x0, y0, width, height);
			break;
		case ELLIPSE:
			g.setColor(lineColor);
			g.drawOval(x0, y0, width, height);
			break;
		case F_ELLIPSE:
			g.setColor(fillColor);
			g.fillOval(x0, y0, width, height);
			break;
		case FB_ELLIPSE:
			g.setColor(fillColor);
			g.fillOval(x0, y0, width, height);
			g.setColor(lineColor);
			g.drawOval(x0, y0, width, height);
		default:
			break;

		}
		repaint();
	}

	// ------------------------------------------------------------
	// Draws figures that require 3 positional arguments (arc
	// is a work in progress)
	// ------------------------------------------------------------
	private void drawFigure(Graphics g, Type t, int x1, int y1, int x2, int y2, int x3, int y3) {
		int xcoords[] = {x1, x2, x3};
		int ycoords[] = {y1, y2, y3};

		double a = x1 - x2;
		double b = y1 - y2;

		double c = Math.sqrt(Math.pow(a,  2) + Math.pow(b,  2));

		double angle = Math.atan(-b/a);
		angle = rad_to_deg(angle);

		switch (t) {
		case TRIANGLE:
			g.setColor(lineColor);
			g.drawPolygon(xcoords, ycoords, 3);
			break;
		case F_TRIANGLE:
			g.setColor(fillColor);
			g.fillPolygon(xcoords, ycoords, 3);
			break;
		case FB_TRIANGLE:
			g.setColor(fillColor);
			g.fillPolygon(xcoords, ycoords, 3);
			g.setColor(lineColor);
			g.drawPolygon(xcoords, ycoords, 3);
			break;
		case ARC:
			g.drawArc(x3, y3, (int)(c), (int)(c), (int)(angle), 180);
			break;
		case F_ARC:
			g.fillArc(x3, y3, (int)(c/2), (int)(c/2), (int)(angle), (int)(angle) +180);
			break;
		default:
			break;
		}
		repaint();
	}

	// ------------------------------------------------------------
	// Draws a freeform curve from a set of coordinates
	// ------------------------------------------------------------
	private void drawFigure(Graphics g, Type t, int[][] coords) {
		int[] xpoints = new int[coords.length];
		int[] ypoints = new int[coords.length];

		for (int i = 0; i < coords.length; i++) {
			xpoints[i] = coords[i][0];
			ypoints[i] = coords[i][1];
		}

		switch (t) {
		case FFC:
			g.setColor(lineColor);
			int[] prevPair = coords[0];
			for (int[] cpair : coords) {
				g.drawLine(prevPair[0], prevPair[1], cpair[0], cpair[1]);
				prevPair = cpair;
			}
			break;
		case C_FFC:
			g.setColor(lineColor);
			g.drawPolygon(xpoints, ypoints, coords.length);
			break;
		case FC_FFC:
			g.setColor(fillColor);
			g.fillPolygon(xpoints, ypoints, coords.length);
			break;
		case FBC_FFC:
			g.setColor(fillColor);
			g.fillPolygon(xpoints, ypoints, coords.length);
			g.setColor(lineColor);
			g.drawPolygon(xpoints, ypoints, coords.length);
			break;
		default:
			break;
		}
		int[] prevPair = coords[1];
		for (int[] cpair : coords) {
			g.drawLine(prevPair[0], prevPair[1], cpair[0], cpair[1]);
			prevPair = cpair;
		}
	}

	// ------------------------------------------------------------
	// Redraws only a specific area of the screen
	// ------------------------------------------------------------
	private void repaintArea(int x1, int y1, int x2, int y2) {
		int x, y;
		int w, h;
		if (x2 >= x1) {
			x = x1;
			w = x2 - x1;
		}
		else {
			x = x2;
			w = x1 - x2;
		}
		if (y2 >= y1) {
			y = y1;
			h = y2 - y1;
		}
		else {
			y = y2;
			h = y1 - y2;
		}
		repaint(x,y,w+1,h+1);
	}




	// ------------------------------------------------------------
	// Converts a radian angle to a degree angle (for use in
	// constructing arcs)
	// ------------------------------------------------------------
	private double rad_to_deg(double radAngle) {
		double degAngle = (radAngle * 180)/3.14159265;
		return degAngle;
	}

	// ------------------------------------------------------------
	// Redraws the entire image using two arraylists (note: this
	// redraws, not repaints)
	// ------------------------------------------------------------
	public void redraw(ArrayList<Layer> layers) {
		Color tempFill = fillColor;
		Color tempLine = lineColor;

		for (Layer currentLayer : layers) {
			if (currentLayer.isVisible()) {
				boolean takes2Coords;
				ArrayList<String> shapes = currentLayer.getFigures();
				ArrayList<String> colors = currentLayer.getColors();
				if (colors != null && shapes != null) {
					for (int i = 0; i < shapes.size(); i++) {
						int x0, y0, x1, y1, x2 = 0, y2 = 0;
						int lc0, lc1, lc2, fc0, fc1, fc2;

						String tempColor = colors.get(i);
						lc0 = Integer.parseInt(tempColor.substring(0, tempColor.indexOf(',')));
						tempColor = tempColor.substring(tempColor.indexOf(',') + 1);
						lc1 = Integer.parseInt(tempColor.substring(0, tempColor.indexOf(',')));
						tempColor = tempColor.substring(tempColor.indexOf(',') + 1);
						lc2 = Integer.parseInt(tempColor.substring(0, tempColor.indexOf(',')));
						tempColor = tempColor.substring(tempColor.indexOf(',') + 1);
						fc0 = Integer.parseInt(tempColor.substring(0, tempColor.indexOf(',')));
						tempColor = tempColor.substring(tempColor.indexOf(',') + 1);
						fc1 = Integer.parseInt(tempColor.substring(0, tempColor.indexOf(',')));
						tempColor = tempColor.substring(tempColor.indexOf(',') + 1);
						fc2 = Integer.parseInt(tempColor);

						fillColor = new Color(fc0, fc1, fc2);
						lineColor = new Color(lc0, lc1, lc2);

						String tempString = shapes.get(i);
						String figure = tempString.substring(0, tempString.indexOf(','));
						Type tempFig = Type.valueOf(figure.toUpperCase());
						tempString = tempString.substring(tempString.indexOf(',') + 1);

						if (tempFig != Type.FFC && tempFig != Type.C_FFC && tempFig != Type.FC_FFC && tempFig != Type.FBC_FFC) {
							x0 = Integer.parseInt(tempString.substring(0, tempString.indexOf(',')));
							tempString = tempString.substring(tempString.indexOf(',') + 1);
							y0 = Integer.parseInt(tempString.substring(0, tempString.indexOf(',')));
							tempString = tempString.substring(tempString.indexOf(',') + 1);
							x1 = Integer.parseInt(tempString.substring(0, tempString.indexOf(',')));
							tempString = tempString.substring(tempString.indexOf(',') + 1);

							if (tempString.indexOf(',') == -1) {
								takes2Coords = true;
								y1 = Integer.parseInt(tempString);
							} else {
								takes2Coords = false;
								y1 = Integer.parseInt(tempString.substring(0, tempString.indexOf(',')));
								tempString = tempString.substring(tempString.indexOf(',') + 1);
								x2 = Integer.parseInt(tempString.substring(0, tempString.indexOf(',')));
								tempString = tempString.substring(tempString.indexOf(',') + 1);
								y2 = Integer.parseInt(tempString);
							}


							if (takes2Coords) {
								drawFigure(offImage.getGraphics(), tempFig, x0, y0, x1, y1);
							} else {
								drawFigure(offImage.getGraphics(), tempFig, x0, y0, x1, y1, x2, y2);
							}
						}
						else {
							tempString = tempString.replaceAll(" ","");
							tempString = tempString.substring(1, tempString.length() - 1) + "~~"; //hacky way to make sure the string is long enough
							int indices = tempString.length() - tempString.replace("[", "").length(); // number of "[" in string
							int[][] tempArray = new int[indices][2];

							for (int count = 0; count < indices; count++) {
								tempArray[count][0] = Integer.parseInt(tempString.substring(1, tempString.indexOf(',')));
								tempArray[count][1] = Integer.parseInt(tempString.substring(tempString.indexOf(',') + 1, tempString.indexOf(']')));
								tempString = tempString.substring(tempString.indexOf(']') + 2);
							}
							drawFigure(offImage.getGraphics(), tempFig, tempArray);
						}
					}
				}
			}
		}
		lineColor = tempLine;
		fillColor = tempFill;
	}
	
	
	private class ColorBox {

		private final int Y_OFFSET = 3;
		private final int X_OFFSET = 4;
		private final int[] CBOX_BG = {228, 228, 228};

		private int cBoxX, cBoxY;
		
		private boolean isVisible = true;

		
		
		private int[] tempColor = new int[3];
		private Color fillColor, lineColor;
		private Color cBox_Color = new Color(228, 228, 228);
		private Paint canvas;
		
		private int[][] colorPalette;

		// ------------------------------------------------------------
		// Constructor
		// ------------------------------------------------------------
		public ColorBox(Paint c, int x, int y, int[][] cp, Color fill, Color line) {
			colorPalette = cp;
			canvas = c;
			cBoxX = x;
			cBoxY = y;
			fillColor = fill;
			lineColor = line;
			
			tempColor[0] = cType.equals("Fill")? fillColor.getRed() : lineColor.getRed();
			tempColor[1] = cType.equals("Fill")? fillColor.getGreen() : lineColor.getGreen();
			tempColor[2] = cType.equals("Fill")? fillColor.getBlue() : lineColor.getBlue();
			
		}

		// ------------------------------------------------------------
		// Draws the box on the screen
		// ------------------------------------------------------------
		public void draw(Graphics g) {
			cBox_Color = new Color(CBOX_BG[0], CBOX_BG[1], CBOX_BG[2]);
			g.setColor(cBox_Color);
			g.fillRect(cBoxX,  cBoxY,  240, 300);
			g.setColor(Color.BLACK);
			g.drawRect(cBoxX,  cBoxY,  240, 300);

			g.drawLine(cBoxX + 140, cBoxY, cBoxX + 140, cBoxY + 300);
			g.drawLine(cBoxX + 200, cBoxY, cBoxX + 200, cBoxY + 200);
			g.drawLine(cBoxX + 220, cBoxY, cBoxX + 220, cBoxY + 200);
			g.drawLine(cBoxX + 240, cBoxY, cBoxX + 240, cBoxY + 200);

			for (int i = 1; i <= 10; i++) {
				g.drawLine(cBoxX + 140, cBoxY + 20 * i, cBoxX + 240, cBoxY + 20 * i);
				g.drawString("S", cBoxX + 200 + X_OFFSET, cBoxY + 20 * i - Y_OFFSET);
				g.drawString("L", cBoxX + 220 + X_OFFSET, cBoxY + 20 * i - Y_OFFSET);
			}

			g.drawRect(cBoxX + 20, cBoxY + 20, 20, 257);
			g.drawRect(cBoxX + 60, cBoxY + 20, 20, 257);
			g.drawRect(cBoxX + 100, cBoxY + 20, 20, 257);

			for (int i = 0; i <= 255; i++) {
				cBox_Color = new Color(i, 0, 0);
				g.setColor(cBox_Color);
				g.drawLine(cBoxX + 21, cBoxY + 21 + i, cBoxX + 39, cBoxY + 21 + i);

				cBox_Color = new Color(0, i, 0);
				g.setColor(cBox_Color);
				g.drawLine(cBoxX + 61, cBoxY + 21 + i, cBoxX + 79, cBoxY + 21 + i);

				cBox_Color = new Color(0, 0, i);
				g.setColor(cBox_Color);
				g.drawLine(cBoxX + 101, cBoxY + 21 + i, cBoxX + 119, cBoxY + 21 + i);
			}



			// Big color sample box
			cBox_Color = new Color(tempColor[0], tempColor[1], tempColor[2]);
			g.setColor(cBox_Color);
			g.fillRect(cBoxX + 141, cBoxY + 201, 99, 59);

			// "load current" text
			g.setColor(Color.BLACK);
			g.drawString("Load current", cBoxX + 140 + X_OFFSET, cBoxY + 280 - Y_OFFSET);

			g.drawRect(cBoxX + 140, cBoxY + 200, 100, 60);
			g.drawString("Apply", cBoxX + 140 + X_OFFSET, cBoxY + 300 - Y_OFFSET);
			g.drawRect(cBoxX + 140, cBoxY + 280, 40, 20);
			g.drawString(cType, cBoxX + 180 + X_OFFSET, cBoxY + 300 - Y_OFFSET);
			g.drawRect(cBoxX + 180, cBoxY + 280, 60, 20);
			g.drawString("OK", cBoxX + 221, cBoxY + 300 - Y_OFFSET);
			g.drawRect(cBoxX + 219, cBoxY + 280, 21, 20);

			g.drawString(">", cBoxX + X_OFFSET, cBoxY + 25 + tempColor[0]);
			g.drawString(">", cBoxX + X_OFFSET + 40, cBoxY + 25 + tempColor[1]);
			g.drawString(">", cBoxX + X_OFFSET + 80, cBoxY + 25 + tempColor[2]);

			// RGB values and the boxes around them
			g.drawString(tempColor[0] + "", 20 + cBoxX, 295 - Y_OFFSET + cBoxY);
			g.drawString(tempColor[1] + "", 60 + cBoxX, 295 - Y_OFFSET + cBoxY);
			g.drawString(tempColor[2] + "", 100 + cBoxX, 295 - Y_OFFSET + cBoxY);
			g.drawRect(cBoxX + 18, cBoxY + 277, 24, 20);
			g.drawRect(cBoxX + 58, cBoxY + 277, 24, 20);
			g.drawRect(cBoxX + 98, cBoxY + 277, 24, 20);

			for (int i = 0; i <= 9; i++) {
				cBox_Color = new Color(colorPalette[i][0], colorPalette[i][1], colorPalette[i][2]);
				g.setColor(cBox_Color);
				g.fillRect(cBoxX + 141, cBoxY + 20 * i + 1, 59, 19);
			}

		}

		// ------------------------------------------------------------
		// Handles input, does actions based on coordinate of user's
		// click relative to the box
		// ------------------------------------------------------------
		public void inputClick(int posX, int posY) {
			
			// Checks if user clicked in color bars
			if (posX >= cBoxX + 20 && posX <= cBoxX + 40 && posY > cBoxY + 20 && posY < cBoxY + 286) {
				tempColor[0] = posY - cBoxY - 21;
			}
			if (posX >= cBoxX + 60 && posX <= cBoxX + 80 && posY > cBoxY + 20 && posY < cBoxY + 286) {
				tempColor[1] = posY - cBoxY - 21;
			}
			if (posX >= cBoxX + 100 && posX <= cBoxX + 120 && posY > cBoxY + 20 && posY < cBoxY + 286) {
				tempColor[2] = posY - cBoxY - 21;
			}

			// Checks if user clicked on "S" or "L" buttons
			for (int i = 1; i <= 10; i++) {
				if (posX > cBoxX + 200 && posY > cBoxY 
						&& posX < cBoxX + 220 && posY < cBoxY + i * 20) {

					colorPalette[i - 1][0] = tempColor[0];
					colorPalette[i - 1][1] = tempColor[1];
					colorPalette[i - 1][2] = tempColor[2];
					i = 11;
				}
			}
			for (int i = 1; i <= 10; i++) {
				if (posX > cBoxX + 220 && posY > cBoxY 
						&& posX < cBoxX + 240 && posY < cBoxY + i * 20) {

					tempColor[0] = colorPalette[i - 1][0];
					tempColor[1] = colorPalette[i - 1][1];
					tempColor[2] = colorPalette[i - 1][2];
					i = 11;
				}
			}

			
			// checks if user clicked "Apply"
			if (posX >= cBoxX + 140 && posX <= cBoxX + 180 && posY >= cBoxY + 280 && posY <= cBoxY + 300) {
				canvas.setColorData(cType, tempColor, colorPalette);
			}
			
			// checks if user clicked "OK"
			if (posX >= cBoxX + 219 && posX <= cBoxX + 241 && posY >= cBoxY + 280 && posY <= cBoxY + 300) {
				canvas.setColorData(cType, tempColor, colorPalette);
				isVisible = false;
			}
			
			// checks if user clicked the color type (fill/line)
			if (posX >= cBoxX + 180 && posX <= cBoxX + 249 && posY >= cBoxY + 280 && posY <= cBoxY + 300) {
				cType = cType.equals("Line")? "Fill" : "Line";
			}
			
			// Checks if user clicked "load current" button
			if (posX >= cBoxX + 140 && posX <= cBoxX + 240 && posY >= cBoxY + 260 && posY <= cBoxY + 280) {
				switch (cType) {
				case "Fill":
					tempColor[0] = fillColor.getRed();
					tempColor[1] = fillColor.getGreen();
					tempColor[2] = fillColor.getBlue();
					break;
				case "Line":
					tempColor[0] = lineColor.getRed();
					tempColor[1] = lineColor.getGreen();
					tempColor[2] = lineColor.getBlue();
					break;
				}
			}
			
			// Checks if user clicked on the numbers below the color bars
			// Uses Math.abs and % 256 to force user input to be in the correct range for a color value
			if (posX >= cBoxX + 18 && posX <= cBoxX + 42 && posY >= cBoxY + 277 && posY <= cBoxY + 297) {
				tempColor[0] = Math.abs(Integer.parseInt(JOptionPane.showInputDialog("Value for red (0-255):"))) % 256;
			}
			if (posX >= cBoxX + 58 && posX <= cBoxX + 82 && posY >= cBoxY + 277 && posY <= cBoxY + 297) {
				tempColor[1] = Math.abs(Integer.parseInt(JOptionPane.showInputDialog("Value for green (0-255):"))) % 256;
			}
			if (posX >= cBoxX + 98 && posX <= cBoxX + 122 && posY >= cBoxY + 277 && posY <= cBoxY + 297) {
				tempColor[2] = Math.abs(Integer.parseInt(JOptionPane.showInputDialog("Value for blue (0-255):"))) % 256;
			}
			
			// Checks if user clicked on a color in the palette
			if (posX >= cBoxX + 140 && posX <= cBoxX + 200 && posY >= cBoxY && posY <= cBoxY + 200) {
				String colString = JOptionPane.showInputDialog("RGB value for color: ");
				colString = colString.replaceAll(" ", "");
				colorPalette[(posY - cBoxY) / 20][0] = Math.abs(Integer.parseInt(colString.substring(0, colString.indexOf(",")))) % 256;
				colString = colString.substring(colString.indexOf(",") + 1);
				colorPalette[(posY - cBoxY) / 20][1] = Math.abs(Integer.parseInt(colString.substring(0, colString.indexOf(",")))) % 256;
				colString = colString.substring(colString.indexOf(",") + 1);
				colorPalette[(posY - cBoxY) / 20][2] = Math.abs(Integer.parseInt(colString)) % 256;
			}

			// returns 1 if the user clicked in the color dialogue box at all
			// (it's so the code in mouseReleased knows not to remove it)
			if (!(posX >= cBoxX && posY >= cBoxY && posX <= cBoxX + 240 && posY <= cBoxY + 300)) {
				isVisible = false;
			}


		}
		
		public boolean isActive() {
			return isVisible;
		}
	}
	
	private class DialogueBox {

		private final int DBOX_HEIGHT = 20;
		private final int DBOX_WIDTH = 100;
		private final int Y_OFFSET = -5;
		private final int X_OFFSET = 4;

		private int dBoxX, dBoxY;

		private String boxCode;

		private ArrayList<String> args = new ArrayList<String>();

		private Color dBox_Color;
		private Paint c;

		// ------------------------------------------------------------
		// Constructor
		// ------------------------------------------------------------
		public DialogueBox(Paint canvas, Graphics page, int x, int y) {
			c = canvas;
			dBoxX = x;
			dBoxY = y;
			dBox_Color = new Color(228, 228, 228);
			boxCode = "0";

			args.add("Choose shape");
			args.add("Choose color");
			args.add("Choose layer");
			args.add("Undo");
			args.add("Reload");
			args.add("Load");
			args.add("Save");
			args.add("Export");
			args.add("Clear");
		}

		// ------------------------------------------------------------
		// Draws the box on the screen
		// ------------------------------------------------------------
		public void draw(Graphics g) {
			g.setColor(dBox_Color);
			g.fillRect(dBoxX, dBoxY, DBOX_WIDTH, args.size() * DBOX_HEIGHT);
			g.setColor(Color.black);
			for (int i = 0; i < args.size(); i++) {
				g.drawRect(dBoxX,  dBoxY + i * DBOX_HEIGHT,  DBOX_WIDTH, DBOX_HEIGHT);
				g.drawString(args.get(i), dBoxX + X_OFFSET, dBoxY + 20 * (i + 1) + Y_OFFSET);
			}
			c.repaint();
		}

		// ------------------------------------------------------------
		// Handles input, does actions based on coordinate of user's
		// click relative to the box
		// ------------------------------------------------------------
		public void inputClick(int posX, int posY) {
			//checks if the cursor coords are within the dbox rectangle
			for (int i = 1; i <= args.size(); i++) {
				if (posX > dBoxX && posY > dBoxY 
						&& posX < dBoxX + DBOX_WIDTH && posY < dBoxY + i * DBOX_HEIGHT) {
					boxCode += (i - 1);
					args.clear();
					switch (boxCode) {
					case "00":
						args.add("Line");
						args.add("Rectangle");
						args.add("Ellipse");
						args.add("Triangle");
						args.add("Arc");
						args.add("Freeform curve");
						break;
					case "000":
						args.add("Line");
						args.add("Straight line");
						break;
					case "001":
					case "002":
					case "003":
					case "004":
						args.add("Border");
						args.add("Filled");
						args.add("Filled bordered");
						break;
					case "005":
						args.add("Open");
						args.add("Closed");
						args.add("Filled");
						args.add("Filled bordered");
						break;
					}
					return;
				}
			}

			boxCode = "";
			args.clear();
		}


		// ------------------------------------------------------------
		// Gets the value that represents what the user clicked on
		// ------------------------------------------------------------
		public String getBoxCode() {
			return boxCode;
		}
	}
	
	private class Layer {

		private ArrayList<String> imageCode;
		private ArrayList<String> colorCode;
		private String name;
		private boolean visible;
		public Object getImage;
		
		// ------------------------------------------------------------
		// Creates a blank layer
		// ------------------------------------------------------------
		public Layer() {
			imageCode = new ArrayList<String>();
			colorCode = new ArrayList<String>();
			name = "Layer " + numLayers;
			visible = true;
			numLayers++;
		}
		
		// ------------------------------------------------------------
		// Deletes the most recent color/shape pair
		// ------------------------------------------------------------
		public void undo() {
			colorCode.remove(colorCode.size() - 1);
			imageCode.remove(imageCode.size() - 1);
		}
		
		// ------------------------------------------------------------
		// Adds a shape to the layer
		// ------------------------------------------------------------
		public void addFigure(String iData) {
			imageCode.add(iData);
		}
		
		// ------------------------------------------------------------
		// Returns the list of shapes in the layer
		// ------------------------------------------------------------
		public ArrayList<String> getFigures() {
			return imageCode;
		}
		
		// ------------------------------------------------------------
		// Adds a color to the layer
		// ------------------------------------------------------------
		public void addColor(String cData) {
			colorCode.add(cData);
		}
		
		// ------------------------------------------------------------
		// Returns the list of colors in the layer
		// ------------------------------------------------------------
		public ArrayList<String> getColors() {
			return colorCode;
		}
		
		// ------------------------------------------------------------
		// Returns the layer name
		// ------------------------------------------------------------
		public String getName() {
			return name;
		}
		
		// ------------------------------------------------------------
		// Sets the layer name
		// ------------------------------------------------------------
		public void setName(String n) {
			name = n;
		}
		
		// ------------------------------------------------------------
		// Returns the visibility of the layer
		// ------------------------------------------------------------
		public boolean isVisible() {
			return visible;
		}
		
		// ------------------------------------------------------------
		// Sets visibility of the layer
		// ------------------------------------------------------------
		public void changeVisibility(boolean v) {
			visible = v;
		}
		
		// ------------------------------------------------------------
		// Toggles visibility of the layer
		// ------------------------------------------------------------
		public void changeVisibility() {
			visible = visible? false : true;
		}
		
		// ------------------------------------------------------------
		// Returns the number of shapes
		// ------------------------------------------------------------
		public int getSize() {
			return imageCode.size();
		}
		
		
	}

	private class LayerBox {
		private int dBoxX, dBoxY, selection;
		private Paint canvas;
		private Color dBox_Color;
		private boolean isVisible;
		private boolean advDialogue;
		private ArrayList<Layer>  layers = new ArrayList<Layer>();


		// ------------------------------------------------------------
		// Constructor
		// ------------------------------------------------------------
		public LayerBox(Paint c, ArrayList<Layer> ls, int x, int y) {
			canvas = c;
			dBoxX = x;
			dBoxY = y;
			dBox_Color = new Color(228, 228, 228);

			layers = ls;
			isVisible = true;
		}

		// ------------------------------------------------------------
		// Draws the box on the screen
		// ------------------------------------------------------------
		public void draw(Graphics g) {
			g.setColor(dBox_Color);
			g.fillRect(dBoxX, dBoxY, 120, layers.size() * 20);
			g.setColor(Color.black);
			g.drawRect(dBoxX, dBoxY, 120, 20 * layers.size());
			g.drawLine(dBoxX + 40, dBoxY, dBoxX + 40, dBoxY + layers.size() * 20);

			g.setColor(Color.black);
			for (int i = 0; i < layers.size(); i++) {
				g.drawRect(dBoxX + 20, dBoxY + (20 * i), 100, 20);
				g.drawString(layers.get(i).getName(), dBoxX + 43, dBoxY + (20 * (i + 1)) - 5);
			}

			g.setColor(Color.yellow);
			for (int i = 0; i < layers.size(); i++) {
				if (layers.get(i).isVisible())
					g.fillRect(dBoxX + 21, dBoxY + (20 * i) + 1, 19, 19);
			}

			g.setColor(dBox_Color);
			g.fillRect(dBoxX, dBoxY + 20 * layers.size(), 120, 20);
			g.setColor(Color.black);
			g.drawRect(dBoxX, dBoxY + 20 * layers.size(), 120, 20);

			if (!advDialogue) {		// New layer "+" box
				g.drawString("+", dBoxX + 60, dBoxY + (20 * (layers.size() + 1)) - 5);
			}
			else {					// Advanced dialogue
				g.drawLine(dBoxX + 30, dBoxY + (20 * layers.size()), dBoxX + 30, dBoxY + (20 * layers.size()) + 20);
				g.drawLine(dBoxX + 60, dBoxY + (20 * layers.size()), dBoxX + 60, dBoxY + (20 * layers.size()) + 20);
				g.drawLine(dBoxX + 90, dBoxY + (20 * layers.size()), dBoxX + 90, dBoxY + (20 * layers.size()) + 20);
				g.drawString("REN", dBoxX + 2, dBoxY + (20 * layers.size()) + 15);
				g.drawString("/\\", dBoxX + 42, dBoxY + (20 * layers.size()) + 15);
				g.drawString("\\/", dBoxX + 72, dBoxY + (20 * layers.size()) + 15);
				g.drawString("DEL", dBoxX + 92, dBoxY + (20 * layers.size()) + 15);
			}

			// displays ">" for the active layer
			if (layers.size() > 0)
				g.drawString(">", dBoxX + 3, dBoxY + canvas.getActiveLayer() * 20 + 15);
		}
		
		// ------------------------------------------------------------
		// Handles input, does actions based on coordinate of user's
		// click relative to the box
		// ------------------------------------------------------------
		public void inputClick(int posX, int posY) {
			if (!(posX >= dBoxX && posY >= dBoxY && posX <= dBoxX + 120 && posY <= dBoxY + 20 + layers.size() * 20)) {
				isVisible = false;
			} 
			else if (posY > dBoxY + (20 * layers.size())) {
				if (!advDialogue) {
					layers.add(new Layer());
					canvas.setLayerData(layers);
				}
				else {
					switch ((posX - dBoxX) / 30) {
					case 0:
						layers.get(selection).setName(JOptionPane.showInputDialog("New name for layer"));
						break;
					case 1:
						// move layer up
						if (selection != 0) {
							Layer tempLayer = layers.get(selection - 1);
							layers.set(selection - 1, layers.get(selection));
							layers.set(selection, tempLayer);
							selection--;
							canvas.setActiveLayer(selection);
						}
						break;
					case 2:
						// move layer down
						if (selection != layers.size() - 1) {
							Layer tempLayer = layers.get(selection + 1);
							layers.set(selection + 1, layers.get(selection));
							layers.set(selection, tempLayer);
							selection++;
							canvas.setActiveLayer(selection);
						}
						break;
					case 3:
						//delete layer
						layers.remove(selection);
						if (layers.size() == 0)
							advDialogue = false;
						break;
					}
					canvas.updateImage();
				}
			} 
			else if (posX < dBoxX + 20) {
				canvas.setActiveLayer((posY-dBoxY) / 20);
			} 
			else if (posX < dBoxX + 40) {
				layers.get((posY-dBoxY) / 20).changeVisibility();
				canvas.updateImage();
			}
			else {
				advDialogue = (advDialogue)? false : true;
				selection = (posY - dBoxY) / 20;
				canvas.setActiveLayer((posY-dBoxY) / 20);
			}
		}

		// ------------------------------------------------------------
		// Returns the value that indicates if the layer box is
		// currently being displayed
		// ------------------------------------------------------------
		public boolean isActive() {
			return isVisible;
		}

	}

	private class LoadDialogue {
		private final int WIDTH = 300;
		private final int HEIGHT = 50;

		private final String DEBUG_0 = "C:/Users/Nikos/Desktop/test.txt";
		private final String DEBUG_1 = "C:/Users/Nikos/Desktop/test1.txt";

		// color data and shapes data, respectively
		private ArrayList<Layer> iData = new ArrayList<Layer>();

		private JFrame frame;
		private JPanel panel;
		private JLabel inputLabel;
		private JTextField input;

		private Paint canvas;


		// ------------------------------------------------------------
		// Constructor
		// ------------------------------------------------------------
		public LoadDialogue(Paint d) {
			canvas = d;
			frame = new JFrame("Open...");
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setResizable(false);

			inputLabel = new JLabel("Enter load location: ");
			input = new JTextField(10);
			input.addActionListener(new EnterPressed());

			panel = new JPanel();
			panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
			panel.add(inputLabel);
			panel.add(input);

			frame.getContentPane().add(panel);
		}

		// ------------------------------------------------------------
		// Displays the frame
		// ------------------------------------------------------------
		public void Display() {
			frame.pack();
			frame.setVisible(true);
		}

		// ------------------------------------------------------------
		// Gets rid of the frame
		// ------------------------------------------------------------
		private void Destroy() {
			frame.dispose();
		}

		// ------------------------------------------------------------
		// Reads a file in and stores it to sData and cData
		// ------------------------------------------------------------
		private void Load(String p) {
			String path = p;
			boolean endBlockReached = false;
			try {
				for (String line : Files.readAllLines(Paths.get(path))) {
					if (line.contains("#####")) {
						iData.add(new Layer());
						iData.get(iData.size() - 1).setName(line.substring(line.indexOf('/') + 1));
						endBlockReached = false;
					} else {
						if (!line.equals("~~~~~") && !endBlockReached) {
							iData.get(iData.size() - 1).addFigure(line);
						} else {
							endBlockReached = true;
						}

						if (!line.equals("~~~~~") && endBlockReached) {
							iData.get(iData.size() - 1).addColor(line);
						}
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// I don't think these are needed. Consider deleting them.
		//	public ArrayList<String> getShapeData() {
		//		return sData;
		//	}
		//
		//	public ArrayList<String> getColorData() {
		//		return cData;
		//	}

		// ************************************************************
		// Manages the enter press.
		// Made a private class so it can access LoadDialogue's
		// variables.
		// ************************************************************
		private class EnterPressed implements ActionListener {
			public void actionPerformed (ActionEvent e) {
				String path = input.getText();

				// checks for debug strings and assigns the appropriate path
				switch (path ){
				case "d":
					path = DEBUG_0;
					break;
				case "d1":
					path = DEBUG_1;
					break;
				}

				Load(path);
				canvas.setImageData(iData);
				Destroy();

			}
		}
	}

	
	private class SaveDialogue {
		private final int WIDTH = 300;
		private final int HEIGHT = 50;

		private final String DEBUG_0 = "C:/Users/Nikos/Documents/d.txt";
		private final String DEBUG_1 = "C:/Users/Nikos/Documents/d1.txt";

		// save as editor code, or Java code
		private boolean saveAsECode;

		// does what it says on the tin
		private String fileName = new String();

		// color data and shapes data, respectively
		private ArrayList<Layer> iData;

		private JFrame frame;
		private JPanel panel;
		private JLabel inputLabel;
		private JTextField input;

		// ------------------------------------------------------------
		// Constructor
		// ------------------------------------------------------------
		public SaveDialogue(ArrayList<Layer> image, boolean t) {
			iData = image;

			saveAsECode = t;

			frame = new JFrame(t ? "Save as..." : "Export to...");
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setResizable(false);

			inputLabel = new JLabel("Enter location (\"PATH/*.txt\"): ");
			input = new JTextField(10);
			input.addActionListener(new EnterPressed());

			panel = new JPanel();
			panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
			panel.add(inputLabel);
			panel.add(input);

			frame.getContentPane().add(panel);
		}

		// ------------------------------------------------------------
		// Displays the frame
		// ------------------------------------------------------------
		public void Display() {
			frame.pack();
			frame.setVisible(true);
		}

		// ------------------------------------------------------------
		// Gets rid of the frame
		// ------------------------------------------------------------
		private void Destroy() {
			frame.dispose();
		}


		// ------------------------------------------------------------
		// Writes the raw ArrayList data used by the Display class
		// to generate an image within the editor to a file
		// ------------------------------------------------------------
		private void Save(PrintWriter f) {
			PrintWriter file = f;
			for (Layer layer : iData) {
				file.print("#####/" + layer.getName() + "\n");
				for (String s : layer.getFigures()) {
					file.print(s + "\n");
				}
				file.print("~~~~~\n");
				for (String c : layer.getColors()) {
					file.print(c + "\n");
				}
				file.print("~~~~~\n");
			}
			file.close();
		}

		// ------------------------------------------------------------
		// Converts the Display-usable image data to Java code and
		// writes it to a file
		// ------------------------------------------------------------
		private void Export(PrintWriter f) {
			PrintWriter file = f;
			String line = new String();
			String coords = new String();
			String tCoords = new String();

			int width, height, x0, y0;

			// Stores the coordinate points
			int[] x = new int[3];
			int[] y = new int[3];

			// Stores the point data as transformations
			// Index 0 isn't actually used
			int[] xt = new int[3];
			int[] yt = new int[3];

			file.print("// ************************************************************\n" +
					"// " + fileName + ".java			Author: Nikodemos Koutsoheras\n// \n" +
					"// Uses JApplet graphics to draw an image.\n" +	
					"// Java Image Manipulation Program (JIMP) auto-generated code. \n" +
					"// ************************************************************\n\n" +
					"import java.awt.Graphics;\n" +
					"import java.awt.Color;\n" +
					"import javax.swing.JApplet;\n" +
					"public class " + fileName + " extends JApplet {\n" +
					"\tpublic void paint(Graphics g) {\n" +
					" \t\tColor c;\n\t\tColor cf;\n\t\tint[] x = new int[3];\n\t\tint[] y = new int[3];\n\t\tint[] pair = new int[2];\n");

			for (Layer layer : iData) {
				ArrayList<String> sData = layer.getFigures();
				ArrayList<String> cData = layer.getColors();
				System.out.println(cData);

				for (int i = 0; i < sData.size(); i++) {

					file.print("\t\tc = new Color(" + cData.get(i).substring(0, ordinalIndexOf(cData.get(i), ",", 3)) + ");\n");
					file.print("\t\tcf = new Color(" + cData.get(i).substring(ordinalIndexOf(cData.get(i), ",", 3) + 1) + ");\n");
					file.print("\t\tg.setColor(c);\n");

					if (sData.get(i).contains("[")) {	// If it contains "[" it uses an array as an argument, so it's a freeform curve type
						String array = sData.get(i).substring(sData.get(i).indexOf(',') + 1);
						array = array.replace("[", "{");
						array = array.replace("]", "}");

						switch (sData.get(i).substring(0, sData.get(i).indexOf(','))) {
						case "FFC":
							file.print("\t\t{\n" + 
									"\t\tint[][] tempArray = " + array + ";\n" +
									"\t\tpair = tempArray[0];\n" +
									"\t\tfor (int[] p : tempArray) {\n" +
									"\t\t\tg.drawLine(pair[0], pair[1], p[0], p[1]);\n" +
									"\t\t\tpair = p;\n" +
									"\t\t}\n\t\t}\n");
							break;
						case "C_FFC":
							file.print("\t\t{\n" + 
									"\t\tint[][] tempArray = " + array + ";\n" +
									"\t\tint[] xcoords = new int[tempArray.length];\n" +
									"\t\tint[] ycoords = new int[tempArray.length];\n" +
									"\t\tfor (int i = 0; i < tempArray.length; i++) {\n" +
									"\t\t\txcoords[i] = tempArray[i][0];\n" +
									"\t\t\tycoords[i] = tempArray[i][1];\n" +
									"\t\t}\n" +
									"\t\tg.drawPolygon(xcoords, ycoords, tempArray.length);\n" +
									"\t\t}\n");
							break;
						case "FC_FFC":
							file.print("\t\t{\n" + 
									"\t\tint[][] tempArray = " + array + ";\n" +
									"\t\tint[] xcoords = new int[tempArray.length];\n" +
									"\t\tint[] ycoords = new int[tempArray.length];\n" +
									"\t\tfor (int i = 0; i < tempArray.length; i++) {\n" +
									"\t\t\txcoords[i] = tempArray[i][0];\n" +
									"\t\t\tycoords[i] = tempArray[i][1];\n" +
									"\t\t}\n" +
									"\t\tg.setColor(cf);\n" +
									"\t\tg.fillPolygon(xcoords, ycoords, tempArray.length);\n" +
									"\t\t}\n");
							break;
						case "FBC_FFC":
							file.print("\t\t{\n" + 
									"\t\tint[][] tempArray = " + array + ";\n" +
									"\t\tint[] xcoords = new int[tempArray.length];\n" +
									"\t\tint[] ycoords = new int[tempArray.length];\n" +
									"\t\tfor (int i = 0; i < tempArray.length; i++) {\n" +
									"\t\t\txcoords[i] = tempArray[i][0];\n" +
									"\t\t\tycoords[i] = tempArray[i][1];\n" +
									"\t\t}\n" +
									"\t\tg.setColor(cf);\n" +
									"\t\tg.fillPolygon(xcoords, ycoords, tempArray.length);\n" +
									"\t\tg.setColor(c);\n" +
									"\t\tg.drawPolygon(xcoords, ycoords, tempArray.length);\n" +
									"\t\t}\n");
							break;
						}
					}
					else if (sData.get(i).length() - sData.get(i).replace(",", "").length() == 4) {	// exports traditional figures
						line = sData.get(i).substring(sData.get(i).indexOf(',') + 1);
						x[0] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						y[0] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						x[1] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						y[1] = Integer.parseInt(line);


						if (x[0] >= x[1]) {
							x0 = x[1];
							width = x[0] - x[1];
						}
						else {
							x0 = x[0];
							width = x[1] - x[0];
						}
						if (y[0] >= y[1]) {
							y0 = y[1];
							height = y[0] - y[1];
						}
						else {
							y0 = y[0];
							height = y[1] - y[0];
						}

						xt[1] = Math.abs(x[0] - x[1]);
						yt[1] = Math.abs(y[0] - y[1]);
						coords = x[0] + ", " + y[0] + ", " + x[1] + ", " + y[1];
						tCoords = x0 + ", " + y0 + ", " + width + ", " + height;


					} else {
						line = sData.get(i).substring(sData.get(i).indexOf(',') + 1);
						x[0] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						y[0] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						x[1] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						y[1] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						x[2] = Integer.parseInt(line.substring(0, line.indexOf(',')));
						line = line.substring(line.indexOf(',') + 1);
						y[2] = Integer.parseInt(line);
					}

					switch (sData.get(i).substring(0, sData.get(i).indexOf(','))) {
					case "LINE":
						file.print("\t\tg.setColor(c);\n" +
									"\t\tg.drawLine(" + coords + ");\n");
						break;
					case "RECT":
						file.print("\t\tg.setColor(c);\n" +
								"\t\tg.drawRect(" + tCoords + ");\n");
//						file.print("\t\tg.drawRect(" + tCoords + ");\n");
						break;
					case "ELLIPSE":
						file.print("\t\tg.setColor(c);\n" +
								"\t\tg.drawOval(" + tCoords + ");\n");
//						file.print("\t\tg.drawOval(" + tCoords + ");\n");
						break;
					case "TRIANGLE":
						// drawPolygon takes 2 arrays instead of integers, so we have to
						// transform the data string into arrays.
						file.print("\t\tg.setColor(c);\n");
						file.print("\t\tx[0] = " + x[0] + ";\n\t\tx[1] = " + x[1] + ";\n\t\tx[2] = " + x[2] + ";\n");
						file.print("\t\ty[0] = " + y[0] + ";\n\t\ty[1] = " + y[1] + ";\n\t\ty[2] = " + y[2] + ";\n");
						file.print("\t\tg.drawPolygon(x, y, 3);\n");
						break;
					case "ARC":
						file.print("\t\tg.drawArc(" + tCoords + ");\n");
						break;
					case "F_RECT":
						file.print("\t\tg.setColor(cf);\n" +
								"\t\tg.fillRect(" + tCoords + ");\n");
						break;
					case "F_ELLIPSE":
						file.print("\t\tg.setColor(cf);\n" +
								"\t\tg.fillOval(" + tCoords + ");\n");
						break;
					case "F_TRIANGLE":
						// fillPolygon takes 2 arrays instead of integers, so we have to
						// transform the data string into arrays.
						file.print("\t\tg.setColor(cf);\n");
						file.print("\t\tx[0] = " + x[0] + ";\n\t\tx[1] = " + x[1] + ";\n\t\tx[2] = " + x[2] + ";\n");
						file.print("\t\ty[0] = " + y[0] + ";\n\t\ty[1] = " + y[1] + ";\n\t\ty[2] = " + y[2] + ";\n");
						file.print("\t\tg.fillPolygon(x, y, 3);\n");
						break;
					case "F_ARC":
						file.print("\t\tg.fillArc(" + tCoords + ");\n");
						break;
					case "FB_RECT":
						file.print("\t\tg.setColor(cf);\n" +
								"\t\tg.fillRect(" + tCoords + ");\n" +
								"\t\tg.setColor(c);\n" +
								"\t\tg.drawRect(" + tCoords + ");\n");
						break;
					case "FB_ELLIPSE":
						file.print("\t\tg.setColor(cf);\n" +
								"\t\tg.fillOval(" + tCoords + ");\n" +
								"\t\tg.setColor(c);\n" +
								"\t\tg.drawOval(" + tCoords + ");\n");
						break;
					case "FB_TRIANGLE":
						// fillPolygon takes 2 arrays instead of integers, so we have to
						// transform the data string into arrays.
						file.print("\t\tg.setColor(cf);\n");
						file.print("\t\tx[0] = " + x[0] + ";\n\t\tx[1] = " + x[1] + ";\n\t\tx[2] = " + x[2] + ";\n");
						file.print("\t\ty[0] = " + y[0] + ";\n\t\ty[1] = " + y[1] + ";\n\t\ty[2] = " + y[2] + ";\n");
						file.print("\t\tg.fillPolygon(x, y, 3);\n");
						file.print("\t\tg.setColor(c);\n");
						file.print("\t\tg.drawPolygon(x, y, 3);\n");
						break;
					case "FB_ARC":
						file.print("\t\tg.fillArc(" + tCoords + ");\n");
						break;
					}

				}
			}




			file.print("\t}\n}");
		}

		// ************************************************************
		// Manages the enter press.
		// ************************************************************
		private class EnterPressed implements ActionListener {
			public void actionPerformed (ActionEvent e) {

				String path = input.getText();
				fileName = path.substring(path.lastIndexOf('/') + 1, (path.lastIndexOf('.') == -1)? path.length() : path.lastIndexOf('.'));

				// checks for debug strings and assigns the appropriate path
				switch (path){
				case "d":
					path = DEBUG_0;
					break;
				case "d1":
					path = DEBUG_1;
					break;
				default:
					if (path.lastIndexOf('.') == -1) {
						path += ".txt";
					}
					break;
				}

				try {
					PrintWriter file = new PrintWriter(path, "UTF-8");
					if (saveAsECode) {
						Save(file);
					} else {
						Export(file);
					}
					file.close();

				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				Destroy();
			}
		}
	}
	
}