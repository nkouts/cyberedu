package applications;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JTextArea;

public class AntivirusMessage extends WindowedApplication {

	private static final long serialVersionUID = 1L;

	public AntivirusMessage(String error, String explanation) {
		JTextArea errorArea = new JTextArea(error);
		errorArea.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		errorArea.setLineWrap(true);
		errorArea.setWrapStyleWord(true);
		errorArea.setEditable(false);
		
		JTextArea explanationArea = new JTextArea(explanation);
		explanationArea.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		explanationArea.setLineWrap(true);
		explanationArea.setWrapStyleWord(true);
		explanationArea.setEditable(false);
		
		setLayout(new GridLayout(0, 1));
		setPreferredSize(new Dimension(350, 200));
		add(errorArea);
		add(explanationArea);
	}
	
	public String toString() {
		return "Malware blocked!";
	}
	
}
