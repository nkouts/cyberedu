package applications;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

public class IntroDialog extends WindowedApplication implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	JLabel title;
	JTextArea description;
	JButton easy, hard;
	JPanel buttonPane;

	public IntroDialog() {
		title = new JLabel("<html><b>Installer Simulator</b></html>");
		title.setHorizontalAlignment(SwingConstants.CENTER);

		description = new JTextArea("Welcome to the Installer Simulator! This"
				+ " game is designed to teach a user how to avoid potentially"
				+ " unwanted programs when downloading and installing software."
				+ " Try to install the game \"Ball Simulator 2000\" by"
				+ " CollisionsLab without encountering any malware along the"
				+ " way!"
				+ "\n\nWritten by Nikodemos Koutsoheras and Claude Zou.\nAll"
				+ " files are available for free at gitlab.com/nkouts/CyberEDU."
				+ "\nLicensed under the GPLv3.");
		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		description.setEditable(false);
		
		easy = new JButton("Easy Mode");
		easy.addActionListener(this);
		easy.setActionCommand("easy");
		hard = new JButton("Hard Mode");
		hard.addActionListener(this);
		hard.setActionCommand("hard");
		
		buttonPane = new JPanel();
		buttonPane.add(easy);
		buttonPane.add(hard);
		
		setLayout(new BorderLayout());
		setBackground(Color.WHITE);
		add(title, BorderLayout.NORTH);
		add(description, BorderLayout.CENTER);
		add(buttonPane, BorderLayout.SOUTH);
		setPreferredSize(new Dimension(400, 200));
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		case "easy":
			install(new WebBrowser());
			install(new TerminalEmulator());
			close();
			break;
		case "hard":
			setEasyMode(false);
			install(new WebBrowser());
			install(new TerminalEmulator());
			close();
			break;
		}
	}
	
}
