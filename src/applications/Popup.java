package applications;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Popup {

	public static void newPopup(String s) {
		//makes a new window because i dont know how to set jpanels to be on top of one another when setLayout is null
		JFrame popup = new JFrame();
		//popup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		popup.pack();
		popup.setVisible(true);
		popup.setResizable(true);
		popup.setSize(700, 150);
		JLabel message = new JLabel();
		message.setText(s);
		message.setForeground(Color.RED);
		message.setHorizontalAlignment(SwingConstants.CENTER);
		message.setBounds(0, 0, 400, 100);
		message.setFont(new Font("Dialog", Font.PLAIN, 12));
		popup.add(message);
	}
}
