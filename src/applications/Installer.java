package applications;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class Installer extends WindowedApplication {

	private static final long serialVersionUID = 1L;
	private String[] questions = {
			"Install Bonzi Buddy",
			"Install Ask Toolbar",
			"Set Internet Explorer as default browser",
			"Don't install Candy Crush Saga",
			"Enable Cortana",
			"Enable Windows Telemetry Plus",
			"Install Windows 10",
			"Don't not sign me up for our newsletter",
			"Send identifying usage info to developers",
			"Purchase Ball Simulator 2000 Pro for $99.99/mo",
			"Disable automatic free trial of Norton Antivirus"};
	private boolean [] answers = {
		false,
		false,
		false,
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		true
	};
	
	public Installer() {
		setLayout(new BorderLayout());
		setContentPane(page1());
	}

	private JPanel page1() {
		JLabel header = new JLabel("Ball Simulator 2000 Setup Wizard");
		header.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		JTextArea desc = new JTextArea("This will install Ball Simulator 2000"
				+ " on your computer. Click \"Next\" to continue or select"
				+ " \"Custom\" for more installation options.");
		desc.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		desc.setLineWrap(true);
		desc.setWrapStyleWord(true);
		desc.setEditable(false);

		JRadioButton simple = new JRadioButton("Xpress Install");
		simple.setBackground(Color.WHITE);
		simple.setActionCommand("simple");
		simple.setSelected(true);
		JRadioButton complex = new JRadioButton("Custom Install");
		complex.setBackground(Color.WHITE);
		complex.setActionCommand("complex");
		ButtonGroup installOptions = new ButtonGroup();
		installOptions.add(simple);
		installOptions.add(complex);
		JPanel installOptionsPane = new JPanel();
		installOptionsPane.setLayout(new GridLayout(0, 1));
		installOptionsPane.add(simple);
		installOptionsPane.add(complex);

		JPanel text = new JPanel();
		text.setBackground(Color.WHITE);
		text.setLayout(new BorderLayout());
		text.add(header, BorderLayout.NORTH);
		text.add(desc, BorderLayout.CENTER);
		text.add(installOptionsPane, BorderLayout.SOUTH);

		JButton next = new JButton("Next >");
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(installOptions.getSelection().getActionCommand());
				if (installOptions.getSelection().getActionCommand().equals("simple")) {
					userError("Blocked attempt to install " + (int)(Math.random() * 100) + " unwanted programs!", "You should always choose the \"expert\" or \"custom\" installation option to avoid the installer surreptitiously bundling unwanted programs.");
				} else {
					setContentPane(page2());
				}
			}
		});
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		JPanel continueButtons = new JPanel();
		continueButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		continueButtons.add(next);
		continueButtons.add(cancel);

		JPanel page = new JPanel();
		page.setLayout(new BorderLayout());
		page.add(new JLabel(new ImageIcon(getClass().getResource("/resources/win.png"))), BorderLayout.WEST);
		page.add(text, BorderLayout.CENTER);
		page.add(continueButtons, BorderLayout.SOUTH);
		System.out.println(page.getWidth() + ", " + page.getHeight());
		return page;
	}


	public JPanel page2() {
		
		JLabel header = new JLabel("Install additional software");
//		header.setPreferredSize(new Dimension(254, 35));
		header.setBackground(Color.WHITE);
		header.setOpaque(true);
		JTextArea desc = new JTextArea("Please select which software you would like to install right now.");
		desc.setLineWrap(true);
		desc.setWrapStyleWord(true);
		desc.setEditable(false);
		JPanel titlePane = new JPanel();
		titlePane.setLayout(new GridLayout(0, 1));
		titlePane.setBackground(Color.WHITE);
		titlePane.add(header);
		titlePane.add(desc);
		
		JPanel optionsPane = new JPanel();
		optionsPane.setBackground(Color.WHITE);
		optionsPane.setLayout(new GridLayout(0, 1));
		JCheckBox[] checkboxes = new JCheckBox[5];
		for (int i = 0; i < 5; i ++) {
			int selection = (int)(Math.random() * questions.length);
			JCheckBox box = new JCheckBox(questions[selection]);
			optionsPane.add(box);
			box.setBackground(Color.WHITE);
			box.setActionCommand("" + selection);
			box.setSelected(!answers[selection]);
			checkboxes[i] = box;
		}
		
		JButton next = new JButton("Next >");
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean passed = true;
				for (JCheckBox box : checkboxes) {
					boolean answer = answers[Integer.parseInt(box.getActionCommand())];
					System.out.println(answer + " : " + box.isSelected());
					if (box.isSelected() != answer) {
						passed = false;
					}
				}
				if (passed) {
					setContentPane(finalPage());
				} else {
					userError("Blocked attempt to install several unwanted programs!", "You should always read the options in the custom installation carefully - you may end up installing software you didn't initially want if you use the default selections.");
				}
			}
		});
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		JPanel continueButtons = new JPanel();
		continueButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		continueButtons.add(next);
		continueButtons.add(cancel);
		
		JPanel page = new JPanel();
		page.setLayout(new BorderLayout());
//		page.setPreferredSize(new Dimension(380, 349));
		page.add(titlePane, BorderLayout.NORTH);
		page.add(optionsPane, BorderLayout.CENTER);
		page.add(continueButtons, BorderLayout.SOUTH);
		return page;
	}
	
	public JPanel finalPage() {
		JLabel header = new JLabel("Installation complete!");
//		header.setPreferredSize(new Dimension(254, 35));
		header.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		JTextArea desc = new JTextArea("Click \"Finish\" to exit the installer.");
		desc.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		desc.setLineWrap(true);
		desc.setWrapStyleWord(true);
		desc.setEditable(false);

		JCheckBox launchProgram = new JCheckBox("Launch Ball Simulator 2000");
		launchProgram.setSelected(true);
		launchProgram.setBackground(Color.WHITE);
		JPanel finishOptionsPane = new JPanel();
		finishOptionsPane.setBackground(Color.WHITE);
		finishOptionsPane.add(launchProgram);

		JPanel text = new JPanel();
		text.setBackground(Color.WHITE);
		text.setLayout(new BorderLayout());
		text.add(header, 
				BorderLayout.NORTH);
		text.add(desc, BorderLayout.CENTER);
		text.add(finishOptionsPane, BorderLayout.SOUTH);

		JButton finish = new JButton("Finish");
		finish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(launchProgram.isSelected());
				if (launchProgram.isSelected()) {
					start(new BouncingBall());
					install(new BouncingBall());
					endGame();
				}
				close();
			}
		});
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		JPanel continueButtons = new JPanel();
		continueButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		continueButtons.add(finish);

		JPanel page = new JPanel();
		page.setLayout(new BorderLayout());
		page.add(new JLabel(new ImageIcon(getClass().getResource("/resources/win.png"))), BorderLayout.WEST);
		page.add(text, BorderLayout.CENTER);
		page.add(continueButtons, BorderLayout.SOUTH);

		return page;
	}
	
	private void setContentPane(JPanel pane) {
		if (getComponents().length > 0) {
			remove(getComponent(0));
		}
		add(pane, BorderLayout.CENTER);
		revalidate();
		repaint();
	}
	
	public boolean getResizable() {
		return false;
	}

	public String toString() {
		return "Setup - Ball Simulator 2000";
	}

}
