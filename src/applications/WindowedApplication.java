package applications;

import javax.swing.JPanel;

import main.DesktopEnvironment;

public abstract class WindowedApplication extends JPanel {

	private static final long serialVersionUID = 1L;
	private DesktopEnvironment desktop;

	public WindowedApplication setDesktop(DesktopEnvironment desktop) {
		this.desktop = desktop;
		return this;
	}
	
	// will be called every game tick
	// implementation optional
	public void update() {}

	public void start(WindowedApplication application) {
		if (desktop != null) {
			desktop.add(application);
		}
	}
	
	public void replace(WindowedApplication oldApp, WindowedApplication newApp) {
		if (desktop != null) {
			desktop.replaceApplication(oldApp, newApp);
		}
	}
	
	public void install(WindowedApplication application) {
		desktop.addApplicationIcon(application);
	}
	
	public void userError(String error, String explanation) {
		desktop.userError(error, explanation);
	}
	
	public void endGame() {
		desktop.endGame();
	}
	
	public void setEasyMode(boolean b) {
		desktop.setEasyMode(b);
	}
	
	public boolean getResizable() {
		return true;
	}
	
	public boolean getClosable() {
		return true;
	}
	
	public String toString() {
		return getClass().getSimpleName();
	}
	
	public void close() {
		desktop.closeApplication(this);
	}

}
