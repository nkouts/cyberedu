package applications;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextArea;

public class TerminalEmulator extends WindowedApplication {

	private static final long serialVersionUID = 1L;
	String cmd = "";
	
	public TerminalEmulator() {
		super();
		setPreferredSize(new Dimension(300, 250));
		setLayout(new BorderLayout());
		JTextArea input = new JTextArea("$ ");
		input.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					e.consume();
					String[] cmdArr = cmd.replace("sudo", "").split(" ");
					switch (cmdArr[0]) {
					case "term":
						start(new TerminalEmulator());
						break;
					case "ball":
						install(new WebBrowser());
						start(new BouncingBall());
						break;
					case "install":
						install(new Paint());
						break;
					default:
						break;
					}
					cmd = "";
					input.setText(input.getText() + "\n$ ");

				} else {
					cmd += e.getKeyChar();
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}
		});
		input.setBackground(Color.BLACK);
		input.setForeground(Color.GREEN);
		input.setLineWrap(true);
		add(input, BorderLayout.CENTER);
	}

}
