package applications;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class EndDialog extends WindowedApplication {
	
	public EndDialog(int score) {
		setLayout(new BorderLayout());
		add(new JLabel("Game over!"), BorderLayout.NORTH);
		JTextArea area = new JTextArea("Nice job! Your score was: " + score);
		area.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		area.setLineWrap(true);
		area.setWrapStyleWord(true);
		area.setEditable(false);
		add(area, BorderLayout.SOUTH);
	}

}
